#!/usr/bin/env bash

database_name=hop-db

folder_database=/absolute/path/hop-ast-hashed-ast-parser/$database_name

script_path=/absolute/path/hop-ast-hashed-ast-parser/retrieve_relative_path.sh

for dir in $folder_database/*; do
    if [ -d "$dir" ]; then
        echo "Begin to parse - $dir"
        for file in $dir/*; do
            echo "Create AST json data for - $file"
            part1=`dirname "$file"`
            echo $part1
            part2=`basename "$file"`
            part3=`sh  $script_path $file`
            relative_path="./"$database_name""$part3"/"$part2
            echo "THIS IS THE RELATIVE PATH ----- > $relative_path"
#            HOPTRACE="j2s:dump j2s:key format:json" hopc -Ox $relative_path -v2 --js-no-header
            HOPTRACE="j2s:dump j2s:key" hopc -Ox $relative_path -v2 --js-no-header
            rm a.o
            rm a.out
        done

        echo "######################"
    fi
done
