#!/usr/bin/env bash

#database_name=hop-db
database_name=$2
#database_name=hop-db-test-simple #For test
#folder_database=/user/hmaurel/home/Documents/Phd/Projects/thesis-projects/web-security-group/hop-ast-code2vec-parser/$database_name
folder_database=$1/$database_name

echo "#########################"
echo "BEGIN GENERATION"
echo "#########################"
echo "database name to parse : " $database_name
echo "folder database : " $folder_database

script_path=/Users/hmaurel/Documents/hop-ast-generator/retrieve_relative_path.sh
script_path=/user/hmaurel/home/Documents/Phd/Projects/thesis-projects/web-security-group/code2vec/hop-ast-generator/retrieve_relative_path.sh

for dir in $folder_database/*; do
    if [ -d "$dir" ]; then
        echo "Begin to parse : $dir"
        for file in $dir/*; do
            echo "Create AST json data for : $file"
            part1=`dirname "$file"`
            echo $part1
            part2=`basename "$file"`
            part3=`sh  $script_path $file`
            cd $1
            echo "CHANGE PATH --> `pwd`"
            relative_path="./"$database_name""$part3"/"$part2
            echo "THIS IS THE RELATIVE PATH ----- > $relative_path"
            #HOPTRACE="j2s:dump j2s:key format:json" hopc -Ox $relative_path -v2 --js-no-header
            HOPTRACE="j2s:dump j2s:key" hopc -Ox $relative_path -v2 --js-no-header
            rm a.o
            rm a.out
        done

        echo "######################"
    fi
done
