#!/usr/bin/env bash

p=$1
if [[ $# -eq 0 ]] ; then
    echo "test mode"
    p=/absolute/path/hop-ast-hashed-ast-parser/hop-db/unsafe/renamedVariable1__CWE_79__object-directGet__regex_FILTER-CLEANING-full_special_chars_filter__Use_untrusted_data_script-doublequoted_Event_Handler.js
fi



IFS='/'
set -- $p
#echo $p

res=""
for arg in $p
do
#    echo "$arg"
#    if [[ "$arg" == "hop-db" || "$arg" == "unsafe" || "$arg" == "safe" ]];  then
    if [[ "$arg" == "unsafe" || "$arg" == "safe" ]];  then
        res=$res"/"$arg
#        echo $res
#    else
#        echo "Strings are not equal"
    fi
done
IFS=" "
echo $res
